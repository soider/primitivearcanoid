# GLOBALS
x = 550
y = 250
dx = 7
dy = 5
gWidth = 0
gHeight = 0
canvas = null
ctx = null
paddleWidth = 550
paddleHeight = 10
paddleX = 0
paddleY = 0
intervalId = null
rightDown = false
leftDown = false

bricks = null
ncolls = 5
nrows = 5
brickWidth = null
padding = 1
brickHeight = 20
bricksCount = 0

init_bricks = ->
    brickWidth = gWidth / ncolls - 1
    bricks = new Array(ncolls)
    for i in  [0...ncolls]
        bricks[i] = new Array(nrows)
        for j in [0...nrows]
            bricks[i][j] = 1
            bricksCount++
    
onKeyUp = (evt) ->
    if evt.keyCode == 39 
        rightDown = false
    if evt.keyCode == 37
        leftDown = false

onKeyDown = (evt) ->
    if evt.keyCode == 39
        rightDown = true
    if evt.keyCode == 37
        leftDown = true

circle = (x, y, r) ->
    ctx.beginPath();
    ctx.arc(x, y, r, 0, Math.PI*2, true)
    ctx.closePath();
    ctx.fill()

rect = (x, y, w, h) ->
    ctx.beginPath();
    ctx.rect(x, y, w, h)
    ctx.closePath();
    ctx.fill()

clear = ->
    ctx.clearRect(0, 0, gWidth, gHeight)

init = ->
    canvas = $('#canvas')[0]
    ctx = canvas.getContext "2d"
    gWidth = document.body.clientWidth - 50
    gHeight = document.body.clientHeight - 50
    canvas.width = gWidth
    canvas.height = gHeight
    intervalId = setInterval(draw, 10)

init_paddle = ->
    paddleX = gWidth / 2
    paddleY = gHeight - paddleHeight

draw = -> 
    clear()
    circle(x, y, 10, 10)
    if leftDown
        paddleX = paddleX - 25
    else if rightDown
        paddleX = paddleX + 25
    if paddleX < 0
        paddleX = 0
    if paddleX > gWidth - paddleWidth
        paddleX = gWidth - paddleWidth
    rect(paddleX, paddleY, paddleWidth, paddleHeight)

    for row, i in bricks
        for col, j in row
            if bricks[i][j]
                br_x = (brickWidth + padding) * i + padding
                br_y = (brickHeight + padding) * j + padding
                rect(br_x, br_y, brickWidth, brickHeight)

    rowHeight = brickHeight + padding
    colWidth = brickWidth + padding
    if y < nrows * rowHeight
        rowNum = Math.floor(y/rowHeight)
        colNum = Math.floor(x/colWidth) 
        if rowNum >= 0 && colNum >= 0 && bricks[colNum][rowNum] == 1
            bricks[colNum][rowNum] = 0
            dy = - dy
            bricksCount--

    if bricksCount
        if x + dx > gWidth or x + dx < 0
            dx = -dx
        if y + dy > gHeight
            if x > paddleX and x < paddleX + paddleWidth
                dy = - dy
                dx = 8 * ((x-(paddleX+paddleWidth/2))/paddleWidth);
            else
                clearInterval(intervalId)
        if y + dy < 0
            dy = -dy
        x += dx;
        y += dy;
    else
        ctx.clearRect(0, 0, gWidth, gHeight)
        console.log("WINNER")
        clearInterval(intervalId)

main = ->
    $(document).keydown onKeyDown
    $(document).keyup onKeyUp
    init()
    init_paddle()
    init_bricks()

$(document).ready main